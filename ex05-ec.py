name = 'Zed A. Shaw'
age = 35 # not a lie
height = 74 # inches
weight = 180 # lbs
eyes = 'Blue'
teeth = 'White'
hair = 'Brown'
weight_kg = weight * 0.453592
height_cm = height * 2.54

print "Let's talk about %s." % name
print "he's %d inches (%f cm) tall." % (height, height_cm)
print "He's %d pounds (%f kg) heavy." % (weight, weight_kg)
print "Actually that's not too heavy."
print "He's got %s eyes and %s hair." % (eyes, hair)
print "His teeth are usually %s depending on the coffee." % teeth

weight_kg = weight * 0.453592
height_cm = height * 2.54

# this line is tricky, try to get it exactly right
print "If I add %d, %d, and %d I get %d." % (
    age, height, weight, age + height + weight)

"""
All the Python format characters

(Old String formatting)

http://docs.python.org/2/library/stdtypes.html#string-formatting

Conversion Flags

    '#'     use alternate form

    '0'     zero-padded for numeric values

    '-'     left adjusted (overrides any 0 conversion)

    ' '     put a blank before any positive number


Conversion Types

    'd'     signed integer

    'i'     signed integer

    'o'     signed octal

    'x'     signed hex (lowercase) 

    'X'     signed hex (uppercase) 

    'e'     floating point exponential (lowercase)

    'E'     floating point exponential (uppercase)

    'f'     floating point decimal

    'F'     floating point decimal

    'g'     floating point (lowercase exponential if exponent less
            than -4, or not less than precision, otherwise uses decimal )

    'G'     floating point (like %g, but will use uppercase exponential)

    'c'     character (will accept in or single char string)

    'r'     string (converts any Python object using repr())

    's'     string (converts any python object using str())

    '%'     % character


"""
