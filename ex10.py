tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = """
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
"""

print tabby_cat
print persian_cat
print backslash_cat
print fat_cat

# extra credit
print '''
* Big
* Obese
* ASAD
* Abbas
* """
'''

has_doubles = 'this is "it"'
has_singles = "this is 'it'"

formatter_r = "I say that %r"
formatter_s = "I say that %s"

print formatter_r % has_doubles
print formatter_s % has_singles
