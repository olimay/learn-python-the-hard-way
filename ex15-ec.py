# uses import from the sys module to get arguments
from sys import argv

# unpack the arguments
script, filename = argv

# open a file specified by variable filename (one of the parameters)
txt = open(filename)

# prints a message and the name of the specified file
print "Here's your file %r:" % filename
# prints the contents of the file
print txt.read()
txt.close()

# asks for user input
print "Type the filename again:"
# stores filename specified by user
file_again = raw_input("> ")

# opens the file specified by file_again
txt_again = open(file_again)

# prints the contents of the file open in txt_again
print txt_again.read()

# extra credit
txt_again.close()
