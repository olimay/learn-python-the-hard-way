from sys import argv

script, input_file = argv

# print all lines in a file f
def print_all(f):
    print f.read()

# return the file pointer to the beginning of the file
def rewind(f):
    f.seek(0)

# print a line number (specified integer) then a line from a file
def print_a_line(line_count, f):
    print line_count, f.readline()

current_file = open(input_file)

print "First let's print the whole file:\n"

# print all lines in current_file
print_all(current_file)

print "Now let's rewind, kind of like a tape."

# go to the beginning of current_file
rewind(current_file)

print "Let's print three lines:"

current_line = 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)

""" Additional Notes

  seek():
    has optional parameter whence
      whence == 0 : index from beginning
      whence == 1 : offset from current point
      whence == 2 : offset from end of file

  tell():
    return file's current position

  f.writelines(list)
    writes a list of strings to a file


  Apparently file objects have file numbers?
    f.fileno(x)
  

"""
